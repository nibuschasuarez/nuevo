import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
//Angular Material//


import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DatosComponent } from './components/datos/datos.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { HabilidadesComponent } from './components/habilidades/habilidades.component';
import { EstudiosComponent } from './components/estudios/estudios.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { FormularioComponent } from './components/formulario/formulario.component';
import { SharedModule } from './components/shared/shared.module';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DatosComponent,
    AgendaComponent,
    HabilidadesComponent,
    EstudiosComponent,
    InicioComponent,
    FormularioComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  

  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
